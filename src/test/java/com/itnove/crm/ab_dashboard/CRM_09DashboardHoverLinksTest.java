package com.itnove.crm.ab_dashboard;

import com.itnove.crm.BaseTest;
import com.itnove.crm.pages.DashBoard;
import com.itnove.crm.pages.HoverLinks;
import com.itnove.crm.pages.LoginPage;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.interactions.Actions;

public class CRM_09DashboardHoverLinksTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        Actions action = new Actions(driver);
        LoginPage login = new LoginPage(driver);
        DashBoard dashboard = new DashBoard(driver);
        HoverLinks links = new HoverLinks(driver);

        // naveguem fins a la web CRM
        driver.navigate().to("http://crm.votarem.lu");

        // comprovem que la pagina de aa_login es carrega
        Assert.assertTrue(login.isLoginPagePresent());

        // introduim usuari i password correcte
        login.loginSubmit("user","bitnami");

        // comprovem que la pagina de ab_dashboard es carrega
        Assert.assertTrue(dashboard.isDashboardPresent(driver, wait));

        // recorrem tots els elements i subelements del menu desktop-toolbar
        links.hoverDesktopToolbarDropLinks(driver, action);

        // comprovem que la quantitat d'elements correspon a les subcategories
        Assert.assertEquals(links.getElements(), links.getIterations());

    }
}

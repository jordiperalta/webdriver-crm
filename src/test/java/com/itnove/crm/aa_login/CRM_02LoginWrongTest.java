package com.itnove.crm.aa_login;

import com.itnove.crm.BaseTest;
import com.itnove.crm.pages.LoginPage;
import org.junit.Assert;
import org.junit.Test;

public class CRM_02LoginWrongTest extends BaseTest {

    public void testErrorMessage(String user, String pwd, LoginPage login){

        login.loginSubmit(user,pwd);

        Assert.assertTrue(login.isErrorMessagePresent(driver, wait));
        Assert.assertEquals(login.errorMessageDisplayed(),
                "You must specify a valid username and password.");
    }

    public void testErrorMessageBlank(LoginPage login){

        String errorMessageBeforeSubmit = login.errorMessageDisplayed();
        login.loginSubmit("","");
        String errorMessageAfterSubmit = login.errorMessageDisplayed();

        Assert.assertEquals(errorMessageBeforeSubmit,errorMessageAfterSubmit);
    }

    @Test
    public void testApp() throws InterruptedException {

        LoginPage login = new LoginPage(driver);
        driver.navigate().to("http://crm.votarem.lu");
        Assert.assertTrue(login.isLoginPagePresent());

        // username wrong, password OK
        testErrorMessage("xxxx","bitnami", login);
        // username OK, password wrong
        testErrorMessage("user","xxxx", login);
        // username wrong, password wrong
        testErrorMessage("xxxx","xxxx", login);
        // username blank, password blank
        testErrorMessageBlank(login);
    }
}

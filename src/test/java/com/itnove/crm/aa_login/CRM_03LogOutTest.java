package com.itnove.crm.aa_login;

import com.itnove.crm.BaseTest;
import com.itnove.crm.pages.DashBoard;
import com.itnove.crm.pages.LoginPage;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.interactions.Actions;

public class CRM_03LogOutTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        LoginPage login = new LoginPage(driver);
        DashBoard dashboard = new DashBoard(driver);
        Actions action = new Actions(driver);

        // naveguem fins a la web CRM
        driver.navigate().to("http://crm.votarem.lu");

        // comprovem que la pagina de aa_login es carrega
        Assert.assertTrue(login.isLoginPagePresent());

        // introduim usuari i password correcte
        login.loginSubmit("user","bitnami");

        // comprovem que la pagina de ab_dashboard es carrega
        Assert.assertTrue(dashboard.isDashboardPresent(driver, wait));

        // despleguem el menu d'usuari i fem click en log out
        login.logoutClick(action, wait);

        // comprovem que la pagina de aa_login es carrega
        Assert.assertTrue(login.isLoginPagePresent());
    }
}
package com.itnove.crm.aa_login;

import com.itnove.crm.BaseTest;
import com.itnove.crm.pages.ForgotPassword;
import com.itnove.crm.pages.LoginPage;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CRM_06ForgotPwdWrongMailTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        LoginPage login = new LoginPage(driver);
        ForgotPassword forgotPwd = new ForgotPassword(driver);
        WebDriverWait wait = new WebDriverWait(driver, 10000);

        driver.navigate().to("http://crm.votarem.lu");

        Assert.assertTrue(login.isLoginPagePresent());

        forgotPwd.forgotPassLink.click();

        Assert.assertTrue(forgotPwd.isForgotPwdFormPresent());

        forgotPwd.forgotPasswordSubmit("user", "user@example.com");

        Assert.assertTrue(forgotPwd.isGenerateSuccessPresent(wait));

        Assert.assertTrue(forgotPwd.recoverMessageDisplayed().contains("Provide both a User Name and an Email Address."));
    }
}

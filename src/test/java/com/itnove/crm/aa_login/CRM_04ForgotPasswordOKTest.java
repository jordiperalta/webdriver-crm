package com.itnove.crm.aa_login;

import com.itnove.crm.BaseTest;
import com.itnove.crm.pages.ForgotPassword;
import com.itnove.crm.pages.LoginPage;
import org.junit.Assert;
import org.junit.Test;

public class CRM_04ForgotPasswordOKTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        LoginPage login = new LoginPage(driver);
        ForgotPassword forgotPwd = new ForgotPassword(driver);

        driver.navigate().to("http://crm.votarem.lu");
        Assert.assertTrue(login.isLoginPagePresent());

        forgotPwd.forgotPassLink.click();
        Assert.assertTrue(forgotPwd.isForgotPwdFormPresent());

        forgotPwd.forgotPasswordSubmit("jordiperalta", "jordi.peralta@outlook.com");
        Thread.sleep(500);

        Assert.assertTrue(forgotPwd.generateSuccess.isDisplayed());

        String recoverMessageDisplayed = forgotPwd.generateSuccess.getText();
        Assert.assertTrue(recoverMessageDisplayed.contains("System is unable to process your request. Please contact the system administrator."));
    }
}
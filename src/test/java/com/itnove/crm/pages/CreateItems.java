package com.itnove.crm.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.File;
import java.util.List;

/**
 * Created by guillem on 01/03/16.
 */
public class CreateItems {

    private WebDriver driver;

    @FindBy(xpath = "//*[@id='pagecontent']/div[1]/h2")
    public WebElement moduleTitle;

    @FindBy(xpath = "//*[@id='ajaxHeader']/nav/div/div[5]/ul/li/a")
    public WebElement createDropdown;

    @FindBy(xpath = "//*[@id='ajaxHeader']/nav/div/div[5]/ul/li/ul/li[1]/a")
    public WebElement createAccountButton;

    @FindBy(id = "name")
    public WebElement inputNameField;

    @FindBy(id = "Accounts0emailAddress0")
    public WebElement inputEmailField;

    @FindBy(xpath = "//*[@id='filename_file'][1]")
    public WebElement fileNameInputField;

    @FindBy(xpath = "//*[@id='SAVE'][1]")
    public WebElement saveButton;

    @FindBy(xpath = "//*[@id='SAVE_HEADER']")
    public WebElement saveCallButton;

    @FindBy(xpath = "//*[@id='pagecontent']/div/h2")
    public WebElement savedAccountLabel;

    @FindBy(xpath = "//*[@id='tab-actions']/a")
    public WebElement actionsDroplistTab;

    @FindBy(xpath = "//*[@id='tab-actions']/ul/li/input[@id='delete_button']")
    public WebElement deleteElementButton;

    private String currentItem = "";
    private int documentStep = 0;

    public CreateItems(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public boolean isPageRequestedPresent(String s) throws InterruptedException {

        System.out.println("CreateItems.java:58 "+moduleTitle.getText());
        if (currentItem.equals("DOCUMENT")){
            if (documentStep == 0) {
                s = "2-LOGO-B_ACTIVA.PNG";
                documentStep ++;
            }
        }
        System.out.println(s);
        return (
            moduleTitle.isDisplayed() &&
            moduleTitle.getText().contains(s)
        );
    }

    public void fillRequiredFields(WebDriver driver, String key, String s) throws InterruptedException {

        currentItem = s;
        if (s.equals("DOCUMENT")) {
            File file = new File("src" + File.separator + "main" + File.separator + "resources" + File.separator + "2-logo-B_activa.png");
            String filename = file.getAbsolutePath();
            fileNameInputField.sendKeys(filename);
        }
        String fieldXPath = "//*[@class='required']/../../div/input[@type='text'][@value='']";
        List<WebElement> fields = driver.findElements(By.xpath(fieldXPath));
        for (int i = 0; i < fields.size(); i ++) {
            WebElement field = fields.get(i);
            if (field.getAttribute("value").equals(""))
                field.sendKeys(key);
            Thread.sleep(750);
        }
        if (s.equals("CALL"))
            saveCallButton.click();
        else
            saveButton.click();
    }

    public void deleteElement(WebDriver driver) throws InterruptedException {

        actionsDroplistTab.click();
        Thread.sleep(500);
        deleteElementButton.click();
        Thread.sleep(500);
        driver.switchTo().alert().accept();
    }

    public void setInputNameField(String data) {

        inputNameField.clear();
        inputNameField.sendKeys(data);
    }

    public void setInputEmailField(String data) {

        inputEmailField.clear();
        inputEmailField.sendKeys(data);
    }
}

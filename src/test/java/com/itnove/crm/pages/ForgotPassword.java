package com.itnove.crm.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class ForgotPassword {

    private WebDriver driver;

    @FindBy(id = "fp_user_name")
    public WebElement forgotPwdUserName;

    @FindBy(id = "fp_user_mail")
    public WebElement forgotPwdUserEmail;

    @FindBy(id = "forgotpasslink")
    public WebElement forgotPassLink;

    @FindBy(xpath = "//*[@id='form'][@class='form-signin passform']")
    public WebElement generatePwdButton;

    @FindBy(xpath = "//*[@id=\"form\"][@class='form-signin passform']")
    public WebElement generatePwdForm;


    @FindBy(xpath = "/html/body")
    public WebElement generateSuccess;

    public ForgotPassword(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public boolean isForgotPwdFormPresent() {
        return (
            forgotPwdUserName.isDisplayed() &&
            forgotPwdUserEmail.isDisplayed() &&
            generatePwdButton.isDisplayed()
        );
    }

    public void forgotPasswordSubmit(String username, String email) throws InterruptedException {
        forgotPwdUserName.clear();
        forgotPwdUserEmail.clear();
        forgotPwdUserName.sendKeys(username);
        forgotPwdUserEmail.sendKeys(email);
        generatePwdForm.submit();
    }

    public boolean isGenerateSuccessPresent(WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(generateSuccess));
        return generateSuccess.isDisplayed();
    }

    public String recoverMessageDisplayed() {
        return generateSuccess.getText();
    }
}

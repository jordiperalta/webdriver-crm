package com.itnove.crm;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;


/**
 * Created by guillem on 29/02/16.
 */
public class BaseTest {
    public RemoteWebDriver driver;
    public WebDriverWait wait;

    @Before
    public void setUp() throws IOException {
        String extension;
        extension = ".exe";
        // extension = "-linux";
        String browser = System.getProperty("browser");
        if (browser != null && browser.equalsIgnoreCase("firefox")) {
            DesiredCapabilities capabilities = DesiredCapabilities.firefox();
            System.setProperty("webdriver.gecko.driver", "src" + File.separator + "main" + File.separator + "resources" + File.separator + "geckodriver" + extension);
            driver = new FirefoxDriver(capabilities);
        } else {
            DesiredCapabilities capabilities = DesiredCapabilities.chrome();
            System.setProperty("webdriver.chrome.driver", "src" + File.separator + "main" + File.separator + "resources" + File.separator + "chromedriver" + extension);
            driver = new ChromeDriver(capabilities);
        }

        wait = new WebDriverWait(driver, 10);
//        driver.manage().window().maximize();
        driver.manage().window().fullscreen();

    }

    @After
    public void tearDown() {
        driver.quit();
    }

}

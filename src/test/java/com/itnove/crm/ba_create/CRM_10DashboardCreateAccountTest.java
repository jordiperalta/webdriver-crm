package com.itnove.crm.ba_create;

import com.itnove.crm.BaseTest;
import com.itnove.crm.pages.CreateItems;
import com.itnove.crm.pages.DashBoard;
import com.itnove.crm.pages.LoginPage;
import org.junit.Assert;
import org.junit.Test;

public class CRM_10DashboardCreateAccountTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        LoginPage login = new LoginPage(driver);
        DashBoard dashboard = new DashBoard(driver);
        CreateItems createAccount = new CreateItems(driver);

        driver.navigate().to("http://crm.votarem.lu");
        Assert.assertTrue(login.isLoginPagePresent());

        login.loginSubmit("user","bitnami");
        Assert.assertTrue(dashboard.isDashboardPresent(driver, wait));
        Thread.sleep(500);

        createAccount.createDropdown.click();
        Thread.sleep(500);

        createAccount.createAccountButton.click();
        Thread.sleep(500);

        Assert.assertTrue(createAccount.isPageRequestedPresent("CREATE"));

        createAccount.setInputNameField("UserName");
        createAccount.setInputEmailField("user@example.com");
//        createAccount.saveButton.click();

        Thread.sleep(2000);
        Assert.assertTrue(createAccount.savedAccountLabel.getText().contains("SAVE ACCOUNT"));
    }
}

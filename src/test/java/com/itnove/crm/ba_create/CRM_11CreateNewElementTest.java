package com.itnove.crm.ba_create;

import com.itnove.crm.BaseTest;
import com.itnove.crm.pages.CreateItems;
import com.itnove.crm.pages.DashBoard;
import com.itnove.crm.pages.HoverLinks;
import com.itnove.crm.pages.LoginPage;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.interactions.Actions;

public class CRM_11CreateNewElementTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        Actions action = new Actions(driver);
        LoginPage login = new LoginPage(driver);
        DashBoard dashboard = new DashBoard(driver);
        HoverLinks links = new HoverLinks(driver);
        CreateItems create = new CreateItems(driver);

        String keyname = "Troll Dancer";
        String options[] = {"ACCOUNT", "CONTACTS", "OPPORTUNITY", "LEADS", "DOCUMENT", "CALL", "TASK"};
        int index = 1;


        // naveguem fins a la web CRM
        driver.navigate().to("http://crm.votarem.lu");

        // comprovem que la pagina de aa_login es carrega
        Assert.assertTrue(login.isLoginPagePresent());

        // introduim usuari i password correcte
        login.loginSubmit("user","bitnami");

        // comprovem que la pagina de ab_dashboard es carrega
        Assert.assertTrue(dashboard.isDashboardPresent(driver, wait));

        // comprovem que es mostra la barra de menu superior
        Assert.assertTrue(links.createDropdown.isDisplayed());

        for (String option: options ) {

            if (option != "OPPORTUNITY") {

                // fem hover sobre el tag "ba_create" i click sobre "Create Account"
                links.hoverCreateClickAccountLink(driver, action, index);

                // comprovem que es mostra la pagina de crear element
                Assert.assertTrue(create.isPageRequestedPresent("CREATE"));

                // omplim els camps requerits amb keyname
                create.fillRequiredFields(driver, keyname, option);

                // comprovem que es mostra la pagina amb el titol de key name
                Assert.assertTrue(create.isPageRequestedPresent(keyname.toUpperCase()));

                // eliminem el element creat a la prova (hem de ser nets i polits)
                create.deleteElement(driver);

                // comprovem que es mostra la pagina de crear element
                Assert.assertTrue(create.isPageRequestedPresent(option));

            }
            index ++;
        }
    }
}
package com.itnove.crm.ca_links;

import com.itnove.crm.BaseTest;
import com.itnove.crm.pages.DashBoard;
import com.itnove.crm.pages.LoginPage;
import org.junit.Assert;
import org.junit.Test;

public class CRM_13DashboardMenuMarketingTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        LoginPage login = new LoginPage(driver);
        DashBoard dashboard = new DashBoard(driver);

        driver.navigate().to("http://crm.votarem.lu");
        Assert.assertTrue(login.isLoginPagePresent());

        login.loginSubmit("user","bitnami");
        Assert.assertTrue(dashboard.isDashboardPresent(driver, wait));

        int iterations = 0;

        for (int i = 1; i < dashboard.marketingMenuButtons.size() + 1; i ++) {
            dashboard.marketingDropdown.click();
            Thread.sleep(500);

            driver.findElementByXPath("//*[@id='grouptab_1']/../ul/li["+i+"]").click();
            Thread.sleep(500);

            iterations ++;
        }

        Assert.assertEquals(dashboard.marketingMenuButtons.size(), iterations);
    }
}
